class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  set name(name) {
    return (this._name = name);
  }

  get age() {
    return this._age;
  }
  set age(age) {
    return (this._age = age);
  }

  get salary() {
    return this._salary;
  }
  set salary(salary) {
    return (this._salary = salary);
  }
}

class Programmer extends Employee {
  constructor({ name, age, salary, lang = ["uk"] } = {}) {
    super(name, age, salary);
    this.lang = lang;
  }

  get salary() {
    return super.salary * 3;
  }
}

const junior = new Programmer({ name: "Mango", age: 25, salary: 700 });

const middle = new Programmer({
  name: "Poly",
  age: 30,
  salary: 1500,
  lang: ["en", "uk"],
});

const senior = new Programmer({
  name: "Kiwi",
  age: 35,
  salary: 3000,
  lang: ["en", "pl", "uk"],
});

console.log(junior.salary);
console.log(middle);
console.log(senior);
